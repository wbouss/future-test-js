import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Offer} from "../app/app.component";

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  private urlBase = 'http://search-api.fie.future.net.uk/widget.php?id=review&site=TRD&model_name=iPad_Air';

  constructor(private http: HttpClient) { }

  getData(): Observable<any> {
    return this.http.get( this.urlBase);
  }
}
