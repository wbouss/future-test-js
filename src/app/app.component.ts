import {Component, OnInit} from '@angular/core';
import {OfferService} from "../Services/OfferService";

export interface OfferData {
  link: string;
  display_name: string;
  price: string;
  currency_iso: number;
}

export interface Merchant {
  name: string;
  logo_url: string;
}

export interface Offer {
  merchant: Merchant;
  offer: OfferData;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'my-app-js';
  public offers: Array<Offer> = [];

  constructor(private offerService: OfferService) {
  }

  ngOnInit(): void {
    this.offerService.getData().subscribe( value => {
      this.offers = value['widget'].data.offers;
    });
  }
}
